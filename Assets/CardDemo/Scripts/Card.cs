using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
using Mirror.RemoteCalls;

public class Card : MonoBehaviour
{

    public Animation cardAnim;
    public List<string> animName;

    public Text cardInfoText;
    public Text cardValueText;

    public Button cardBtn;


    public void InitCard()
    {
        cardBtn.interactable = true;
    }
    public void assignCardData(int cardValue, string cardInfo)
    {
        cardInfoText.text = cardInfo;
        cardValueText.text = cardValue + "";
    }

    public void OnCardBtnClicked()
    {
        Debug.Log("btn click");

        cardBtn.interactable = false;
        cardFlip();
    }
    public void cardFlip()
    {
        cardAnim.Play(animName[0]);
    }
    public void cardFlipBack()
    {
        cardAnim.Play(animName[1]);
    }
}
