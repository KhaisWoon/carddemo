using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Mirror;


public class CardDemoNetworkManager : NetworkManager
{
    GameObject maincam;
    // [SerializeField] private GameObject canvas;
    public override void OnStartServer()
    {
        base.OnStartServer();
    }

    public override void OnStopServer()
    {
        base.OnStopServer();
    }
    public override void OnServerDisconnect(NetworkConnection conn)
    {
    }

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
       // Camera.main.enabled = false;
        //base.OnServerAddPlayer(conn);
        GameObject player = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
        NetworkServer.AddPlayerForConnection(conn, player);
        Debug.Log("Add player");
    }

}
