using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;


public class CardDataBase
{

    public List<string> cardInfo = new List<string>() {
        "STR",
        "INT",
        "DEX",
        "PHYA",
        "MAGA",
        "PHYD",
        "MAGD",
        "SPD",
        "CRI",
        "DMG"
    };
    public List<int> cardValue = new List<int>() {
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
    };
}
public class MyCardPlayer : NetworkBehaviour
{
    CardDataBase cardDataBase;
    public GameObject canvas;
    public List<Card> cardList = new List<Card>();
    public override void OnStartServer()
    {
        cardDataBase = new CardDataBase();
        Debug.Log("On PLayer spawn on the server");
        
        cardList.ForEach(x =>
        {
            x.InitCard();
        });
    }
    public override void OnStartClient()
    {
        if (isLocalPlayer)
        {
            canvas.SetActive(true);
        }
    }
    public void OnCardBtnClicked(int i)
    {
        if (isLocalPlayer)
        {
            Debug.Log(" btn " + i + " clicked");
            callBtnClick(i);
            Debug.Log("Request to server");
            RequestServer(i);
        }
    }

    private void callBtnClick(int i)
    {
        Debug.Log(" on btn " + i + " clicked");
        cardList.ForEach(x =>
           {
               x.cardBtn.interactable = false;
           });
    }

    [Command]
    public void RequestServer(int btnIndex)
    {
        Debug.Log("Receive button request from client");
        int CardValue = cardDataBase.cardValue[Random.Range(0, 10)];
        string CardInfo = cardDataBase.cardInfo[Random.Range(0, 10)];
        ReturnRequestToClient(CardValue, CardInfo, btnIndex);
    }

    [TargetRpc]
    public void ReturnRequestToClient(int cValue, string cInfo, int btnIndex)
    {
        Debug.Log("Receive return from server");
        cardList[btnIndex].assignCardData(cValue, cInfo);
        cardList[btnIndex].cardFlip();
        StartCoroutine(OnCompleteFlip(btnIndex));
    }

    IEnumerator OnCompleteFlip(int btnIndex)
    {
        yield return new WaitForSeconds(3);
        cardList[btnIndex].cardFlipBack();
         StartCoroutine(OnCompleteFlipBack());
    }

    IEnumerator OnCompleteFlipBack()
    {
        yield return new WaitForSeconds(1);
         cardList.ForEach(x =>
        {
            x.InitCard();
        });
    }
}
